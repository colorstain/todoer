from flask import Flask, render_template

from todoer.models import Task, User

app = Flask(__name__)

user = User(1, 'aldo', 'test@test.com', 'Aldo')
tasks = [
    Task(1, 'Add more tasks', 1),
    Task(2, 'Make this beautiful', 1)
]

@app.route('/')
def home():
    return render_template(
        'index.html',
        user=user,
        tasks=tasks
    )
