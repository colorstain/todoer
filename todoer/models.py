from datetime import datetime

class Task(object):

    id = None
    description = None
    completed = False
    created_on = None
    completed_on = None
    user_id = None

    def __init__(self, id, description, user_id):
        self.id = id
        self.description = description
        self.user_id = user_id
        # use now to record when this was created
        self.created_on = datetime.now()


class User(object):

    id = None
    username = None
    email = None
    created_on = None
    display_name = None

    def __init__(self, id, username, email, display_name):
        self.id = id
        self.username = username
        self.display_name = display_name
        self.email = email
        # use now to record when this was created
        self.created_on = datetime.now()


